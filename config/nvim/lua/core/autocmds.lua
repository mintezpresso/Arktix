-----------------------------------------------------------
-- Autocommand functions
-----------------------------------------------------------

-- Define autocommands with Lua APIs
-- See: h:api-autocmd, h:augroup

local augroup = vim.api.nvim_create_augroup   -- Create/get autocommand group
local autocmd = vim.api.nvim_create_autocmd   -- Create autocommand

-- General settings:
--------------------

-- Remove whitespace on save
autocmd('BufWritePre', {
  pattern = '',
  command = ":%s/\\s\\+$//e"
    -- still need another one to remove all trailing lines
})

-- Don't fucking auto comment on new line
autocmd('Filetype', {
  pattern = '',
  command = 'set fo-=cro'
})

autocmd('BufWritePost', {
  pattern = '/home/zizi/.local/src/*/config.h',
  command = "!sudo make install"
})

-- Auto update lf config for root on save
autocmd('BufWritePost', {
    pattern = 'lfrc',
    command = "!sudo rm -f /root/.config/lf/lfrc;sudo cp /home/zizi/.config/lf/lfrc /root/.config/lf/lfrc; dunstify 'nvim autocmd' 'lf config updated for all users'; xdotool key Enter"
})

-- Auto compile dwmblocks after edit
autocmd('BufWritePost', {
    pattern = '~/.local/src/arkde/dwmblocks/config.h',
    command = "!cd ~/.local/src/arkde/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }"
})

-- Auto compile dwmblocks after edit
autocmd('BufWritePost', {
    pattern = '~/.local/src/arkde/dwm/config.h',
    command = "!cd ~/.local/src/arkde/dwmblocks/; sudo make install && { killall -q dwmblocks;setsid dwmblocks & }"
})

-- Auto refresh dwmblocks after updating module
autocmd('BufWritePost', {
    pattern = "sb-*",
    command = "!pkill -RTMIN+8 dwmblocks;sleep 0.01;xdotool key Enter",
--    opts = { silent = true }
})

-- Reload GRUB and remove echo commands from grub.cfg
autocmd('BufWritePost', {
    pattern = '/etc/default/grub',
    command = "!sudo grub-mkconfig -o /boot/grub/grub.cfg; sed -i \"s/echo.*\'$//g\" /boot/grub/grub.cfg"
})

-- Advanced
-- https://www.youtube.com/watch?v=9gUatBHuXE0
