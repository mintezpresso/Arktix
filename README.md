## My desktop Linux build <a href="https://artixlinux.org" ><img src="https://img.shields.io/badge/OS-Artix%20Linux-blue?logo=Artix+Linux" alt="OS" /></a>
- Confirmed working on
    - Artix (OpenRC + runit)
    - Void
    - Gecko rolling barebones
    - Debian 11
    - Ubuntu server
    - RHEL

## User tools

- Text editor: ``nvim`` with lua
- File manager: ``lf`` and helper [`lfrun`](https://codeberg.org/mintezpresso/scripts/src/branch/main/utils/lfrun)
- RSS reader: ``newsboat``
- Image viewer: ``nsxiv``
- PDF reader: ``zathura``
- Calendar: [`calmenu`](https://codeberg.org/mintezpresso/scripts/src/branch/main/menu/calmenu)
- Browser: ``firefox``
- Password: [`rbw`](https://github.com/doy/rbw) and helper [`rbwmenu`](https://codeberg.org/mintezpresso/rbwmenu)
- Clipboard: ``clipmenu``
- Search & bookmark: [`searchmenu`](https://codeberg.org/mintezpresso/searchmenu)
- Screenshot: ``scrot`` and helper [`scrotshot`](https://codeberg.org/mintezpresso/scripts/src/branch/main/utils/scrotshot)
- Media player:
    - Video: ``mpv`` with lua
    - Music: ``mpd``
    - Helper: [`ameco`](https://codeberg.org/mintezpresso/scripts/src/branch/main/media/ameco)

## System tools

- DE/WM: [`ArkDE`](https://codeberg.org/mintezpresso/arkde)
- Shell: ``bash`` and dwm shell ``dash``
- Window system: ``X11``
- Compositor: ``picom``
- Wallpaper: ``feh``
- Audio: ``pipewire``
- Wifi: ``connman``
- Bluetooth: ``bluez``
- Cron: ``dcron``
- System fetch: ``pfetch``
- Process manager: ``htop`` and helper [`killpmenu`](https://codeberg.org/mintezpresso/scripts/src/branch/main/menu/killpmenu)

### Other resources
- Scripts
    - Normal scripts: [here](https://codeberg.org/mintezpresso/scripts)
    - Statusbar (dwmblocks) scripts: [here](https://codeberg.org/mintezpresso/ArkDE/src/branch/main/statusbar)

## Notes
- Out of box package count: 845
