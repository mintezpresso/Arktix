#!/bin/sh
# Install all dotfiles
# mintezpresso

# TODO: check PWD
# PWD must be whatever/dotfiles, meaning install.sh should only be ran if user is in dotfiles dir. otherwise shits wont work

[ "$(whoami)" = "root" ] && printf "Don't run this thing as root\n" && dunstify -t 10000 "Don't run this thing as root" && exit 1 # dont run the thing as root

printf "Running this install script will overwrite some user and system config files.\n
Type (y) if you are willing to let the overwrite happen. I suggest you read the script carefully before doing this\n
Type (n) to exit\n\n"

printf "Confirm installing mintezpresso's dotfiles ? (y/n) "
read confirmrun
[ "$confirmrun" = "y" ] || exit

printf "Now installing\n"

############### User directory ~/ ###############

rm "$HOME"/.bashrc
sudo cp $PWD/dotfiles/config/init/.bashrc /etc/bash/bashrc

for initfile in ".xinitrc" ".bash_profile" ".bash_comp" # ".fehbg" # You don't need my wallpaper for the build
do
    cp config/init/"$initfile" $HOME/"$initfile"
done

printf "Done: configs\n"

############### .config update ###############

# Quick full folder install

for scriptconfig in mediamenu calmenu mojimenu rbw
do
    rm -rf $HOME/.config/"$scriptconfig"
    cp -r "$arktix"/config/ "$HOME"/.config/"$scriptconfig"/
    sudo cp -r "$arktix"/config/ root/.config/"$scriptconfig"/
done

for config in dunst dmenu-translate neofetch newsboat nvim lf mpv zathura spotifyd spotify-tui ytsub when picom mpd ncmpcpp cmus qutebrowser mangal
do
    rm -r .config/"$config"
    cp -r "$arktix"/config/ "$HOME"/.config/"$config"/
    sudo cp -r "$arktix"/config/ root/.config/"$config"/
done

printf "Done\n"
