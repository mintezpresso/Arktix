# README for bookmarks

Except for "text" which belongs to my private text dir ~/Text, Each of these bookmark folders serve a certain script

In details:

### zsm

- ssh: [zsm](https://codeberg.org/mintezpresso/zsm)

### searchmenu bookmarks

**Folders**

- messenger: [fbmess](https://codeberg.org/mintezpresso/searchmenu/src/branch/main/modules/fbmess)
- reddit: [redditsearch](https://codeberg.org/mintezpresso/searchmenu/src/branch/main/modules/redditsearch)
- gitsearch: [gitsearch](https://codeberg.org/mintezpresso/searchmenu/src/branch/main/modules/gitsearch) and [gitmenu](https://codeberg.org/mintezpresso/scripts/src/branch/main/git/gitmenu)
- tor: [torsearch](https://codeberg.org/mintezpresso/searchmenu/src/branch/main/modules/torsearch)
- yt: [ytsearch](https://codeberg.org/mintezpresso/searchmenu/src/branch/main/modules/ytsearch)

**Files**

- engine: searchmenu's engine list
- link: searchmenu's bookmark link list
